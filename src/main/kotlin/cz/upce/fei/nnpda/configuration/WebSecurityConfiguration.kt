package cz.upce.fei.nnpda.configuration

import cz.upce.fei.nnpda.component.JwtAuthEntryPoint
import cz.upce.fei.nnpda.component.JwtAuthTokenFilter

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfiguration(
    private val userDetailsService: UserDetailsService,
    private val unauthorizedHandler: JwtAuthEntryPoint,
    private val jwtAuthTokenFilter: JwtAuthTokenFilter
) : WebSecurityConfigurerAdapter() {
    @Bean
    fun authenticationJwtTokenFilter() = jwtAuthTokenFilter

    @Throws(Exception::class)
    public override fun configure(authenticationManagerBuilder: AuthenticationManagerBuilder) {
        authenticationManagerBuilder
            .userDetailsService(userDetailsService)
            .passwordEncoder(passwordEncoder())
    }

    @Bean
    @Throws(Exception::class)
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    @Bean
    fun passwordEncoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.cors().and().csrf().disable().authorizeRequests()
            .antMatchers(Permited.LOGIN, Permited.REGISTER, Permited.SWAGGER_UI, Permited.PASSWORD,
                Permited.API_DOCS, Permited.SWAGGER_HTML, Permited.SWAGGER_RESOURCES)
            .permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }

    private object Permited {
        const val LOGIN = "/user/auth/login"
        const val PASSWORD = "/user/password/**"
        const val REGISTER = "/user/auth/register"
        const val SWAGGER_UI = "/swagger-ui/**"
        const val SWAGGER_HTML = "/swagger-ui.html"
        const val SWAGGER_RESOURCES = "/webjars/swagger-ui/**"
        const val API_DOCS = "/v3/api-docs/**"
    }
}