package cz.upce.fei.nnpda.configuration

import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.security.SecurityScheme
import org.springframework.context.annotation.Configuration


@Configuration
@SecurityScheme(
    name = "bearerAuth", type = SecuritySchemeType.HTTP,
    bearerFormat = "JWT", scheme = "bearer"
)
@OpenAPIDefinition(
    info = Info(
        title = "NNPDA - Semestrální práce A",
        description = "Pro klientskou aplikaci vytvořte REST API obsahující Požadované endpointy.",
        contact = Contact(name = "Ondřej Chrbolka", email = "ondrej.chrbolka@gmail.com"),
        version = "1.0"
    )
)
class OpenApiConfiguration