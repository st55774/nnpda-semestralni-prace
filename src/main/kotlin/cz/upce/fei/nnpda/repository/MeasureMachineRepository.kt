package cz.upce.fei.nnpda.repository

import cz.upce.fei.nnpda.entity.MeasureMachine
import cz.upce.fei.nnpda.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface MeasureMachineRepository : JpaRepository<MeasureMachine, Long> {
    fun findAllByUserEquals(user: User) : List<MeasureMachine>
    fun findByIdAndUserEquals(id : Long, user : User) : Optional<MeasureMachine>
}