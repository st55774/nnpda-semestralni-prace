package cz.upce.fei.nnpda.repository

import cz.upce.fei.nnpda.entity.Sensor
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface SensorRepository : JpaRepository<Sensor, Long>{
    @Query("select s.id from Sensor s")
    fun findIdSet() : List<Long>
}