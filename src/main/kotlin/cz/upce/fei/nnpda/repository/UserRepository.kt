package cz.upce.fei.nnpda.repository

import cz.upce.fei.nnpda.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface UserRepository : JpaRepository<User, Long>{
    fun findByEmail(email: String) : Optional<User>
    fun existsByEmail(email: String) : Boolean
    fun existsByEmailAndPassword(email : String, password : String)
}