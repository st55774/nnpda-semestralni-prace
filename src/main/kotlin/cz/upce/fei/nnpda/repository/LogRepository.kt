package cz.upce.fei.nnpda.repository

import cz.upce.fei.nnpda.entity.Log
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.web.bind.annotation.CrossOrigin

@RepositoryRestResource(collectionResourceRel = "logs", path = "logs")
@Tag(name = "Logs", description = "logs")
@SecurityRequirement(name = "bearerAuth")
@CrossOrigin("http://localhost:3000")
interface LogRepository : JpaRepository<Log, Long>