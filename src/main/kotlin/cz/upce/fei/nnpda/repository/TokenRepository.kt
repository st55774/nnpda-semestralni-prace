package cz.upce.fei.nnpda.repository

import cz.upce.fei.nnpda.entity.Token
import cz.upce.fei.nnpda.entity.User
import org.springframework.data.jpa.repository.JpaRepository
import java.util.*

interface TokenRepository : JpaRepository<Token, Long> {
    fun findByValueAndUser(value : String, user : User) : Optional<Token>
}