package cz.upce.fei.nnpda.component.loader

import cz.upce.fei.nnpda.entity.MeasureMachine
import cz.upce.fei.nnpda.entity.Sensor
import cz.upce.fei.nnpda.entity.SensorType
import cz.upce.fei.nnpda.entity.User
import cz.upce.fei.nnpda.repository.MeasureMachineRepository
import cz.upce.fei.nnpda.repository.SensorRepository
import cz.upce.fei.nnpda.repository.UserRepository
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component

/**
 * Class for creating default users when application start for the first time
 * */
@Component
class UserCreator(
    private val userRepository: UserRepository,
    private val measureMachineRepository: MeasureMachineRepository,
    private val sensorRepository: SensorRepository,
    private val passwordEncoder: PasswordEncoder
) {
    /**
     * Bean which will create default users
     *
     * @return runner containing logic for creating users
     * */
    @Bean
    fun create() = CommandLineRunner {
        listOf(
            User(email = "ondrej.chrbolka@gmail.com", password = passwordEncoder.encode("root")),
            User(email = "ondrej.chrbolka@live.com", password = passwordEncoder.encode("user"))
        ).filter { !userRepository.existsByEmail(it.email) }
            .forEach { user ->
                userRepository.save(user)

                val sensors = mutableSetOf(
                    Sensor(description = "Sensor ${next++}", type = SensorType.ELECTRICITY),
                    Sensor(description = "Sensor ${next++}", type = SensorType.WATER),
                    Sensor(description = "Sensor ${next++}", type = SensorType.AIR)
                )

                val sensors2 = mutableSetOf(
                    Sensor(description = "Sensor ${next++}", type = SensorType.ELECTRICITY),
                    Sensor(description = "Sensor ${next++}", type = SensorType.WATER),
                    Sensor(description = "Sensor ${next++}", type = SensorType.AIR)
                )

                val sensors3 = mutableSetOf(
                    Sensor(description = "Sensor ${next++}", type = SensorType.ELECTRICITY),
                    Sensor(description = "Sensor ${next++}", type = SensorType.WATER),
                    Sensor(description = "Sensor ${next++}", type = SensorType.AIR)
                )

                sensors.forEach { sensorRepository.save(it) }
                sensors2.forEach { sensorRepository.save(it) }
                sensors3.forEach { sensorRepository.save(it) }

                val measureMachines = setOf(
                    MeasureMachine(description = "Kitchen ${user.email}", user = user, sensors = sensors),
                    MeasureMachine(description = "Living room ${user.email}", user = user, sensors = sensors2),
                    MeasureMachine(description = "Bathroom ${user.email}", user = user, sensors = sensors3),
                )
                measureMachineRepository.saveAll(measureMachines)
            }
    }

    private companion object {
        var next = 0
    }
}