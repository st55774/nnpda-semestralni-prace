package cz.upce.fei.nnpda.component

import cz.upce.fei.nnpda.service.security.UserDetailsService
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Authentication filter based on JWT tokens
 * */
@Component
class JwtAuthTokenFilter(
    private val tokenTokenUtility: JwtTokenUtility,
    private val userDetailsService: UserDetailsService
) : OncePerRequestFilter() {
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        authenticate(request)
        filterChain.doFilter(request, response)
    }

    /**
     * Start process of authentication
     *
     * @param request HTTP request
     * */
    private fun authenticate(request: HttpServletRequest) {
        val jwt = getJwt(request)

        try {
            if (jwt != null && tokenTokenUtility.validateJwtToken(jwt)) {
                val username: String = tokenTokenUtility.getUserNameFromJwtToken(jwt)
                val userDetails = userDetailsService.loadUserByUsername(username)

                setSecurityContext(userDetails, request)
            }
        } catch (e: Exception) {
            Companion.logger.error("Can NOT set user authentication -> Message: {}", e)
        }
    }

    /**
     * It will set Spring security context to credentials of new user
     *
     * @param userDetails principals of authenticating user
     * @param request HTTP request
     * */
    private fun setSecurityContext(userDetails: UserDetails, request: HttpServletRequest) {
        val authentication = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
        authentication.details = WebAuthenticationDetailsSource().buildDetails(request)
        SecurityContextHolder.getContext().authentication = authentication
    }

    /**
     * Parse JWT token from HTTP request
     *
     * @param request HTTP request containing Bearer token
     *
     * @return token in String format. Null if token cannot be parsed from request header
     * */
    private fun getJwt(request: HttpServletRequest): String? {
        val authHeader = request.getHeader("Authorization")

        return if (authHeader != null && authHeader.startsWith("Bearer ")) {
            authHeader.replace("Bearer ", "")
        } else null
    }

    companion object {
        private val logger = LoggerFactory.getLogger(JwtAuthTokenFilter::class.java)
    }
}