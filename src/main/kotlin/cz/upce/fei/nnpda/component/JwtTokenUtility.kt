package cz.upce.fei.nnpda.component

import cz.upce.fei.nnpda.service.security.principles.UserPrinciple
import io.jsonwebtoken.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.core.Authentication
import org.springframework.stereotype.Component
import java.util.*

/**
 * Generating and validating JWT token utility
 * */
@Component
class JwtTokenUtility {
    @Value("\${jwt.secret}")
    private val jwtSecret: String = ""

    @Value("\${jwt.expiration}")
    private val jwtExpiration = 0

    /**
     * Generate new JWT token based on user principals
     *
     * @param authentication User authentication contains principals
     *
     * @return JWT token
     * */
    fun generateJwtToken(authentication: Authentication): String {
        val userPrincipal = authentication.principal as UserPrinciple
        return Jwts.builder()
            .setSubject(userPrincipal.username)
            .setIssuedAt(Date())
            .setExpiration(Date(Date().time + jwtExpiration * 1000))
            .signWith(SignatureAlgorithm.HS512, jwtSecret)
            .compact()
    }

    /**
     * Validate JWT token
     *
     * @param authToken string representation token to validate
     *
     * @return true if token is valid
     * */
    fun validateJwtToken(authToken: String): Boolean {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken)
            return true
        } catch (e: SignatureException) {
            logger.error("Invalid JWT signature -> Message: {} ", e)
        } catch (e: MalformedJwtException) {
            logger.error("Invalid JWT token -> Message: {}", e)
        } catch (e: ExpiredJwtException) {
            logger.error("Expired JWT token -> Message: {}", e)
        } catch (e: UnsupportedJwtException) {
            logger.error("Unsupported JWT token -> Message: {}", e)
        } catch (e: IllegalArgumentException) {
            logger.error("JWT claims string is empty -> Message: {}", e)
        }
        return false
    }

    /**
     * Parse username from JWT token
     *
     * @param token containing username of requesting user
     *
     * @return
     * */
    fun getUserNameFromJwtToken(token: String): String = Jwts.parser()
            .setSigningKey(jwtSecret)
            .parseClaimsJws(token)
            .body.subject

    companion object {
        private val logger = LoggerFactory.getLogger(JwtTokenUtility::class.java)
    }
}