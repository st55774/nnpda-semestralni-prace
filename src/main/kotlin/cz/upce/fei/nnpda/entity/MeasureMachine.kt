package cz.upce.fei.nnpda.entity

import javax.persistence.*

@Entity
data class MeasureMachine(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id : Long = 0,
    @Column(length = MAX_LENGTH.toInt()) var description : String,
    @ManyToOne var user : User,
    @OneToMany(fetch = FetchType.EAGER) val sensors : MutableSet<Sensor> = mutableSetOf()
) {
    companion object{
        const val MAX_LENGTH = 255L
    }
}