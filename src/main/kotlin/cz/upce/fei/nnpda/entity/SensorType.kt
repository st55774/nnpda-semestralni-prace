package cz.upce.fei.nnpda.entity

enum class SensorType {
    ELECTRICITY, WATER, AIR
}
