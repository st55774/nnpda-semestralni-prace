package cz.upce.fei.nnpda.entity

import javax.persistence.*

@Entity
data class Sensor(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id : Long = 0,
    @Column(length = MeasureMachine.MAX_LENGTH.toInt()) var description : String,
    @Column @Enumerated(EnumType.STRING) val type : SensorType = SensorType.ELECTRICITY
){
    companion object{
        const val MAX_LENGTH = 255L
    }
}