package cz.upce.fei.nnpda.entity

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity

data class User(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id : Long = 0,
    @Column(nullable = false) @JsonIgnore var password : String = "",
    @Column(nullable = false, unique = true)  val email : String = "",
){
    companion object{
        const val MIN_PASSWORD = 9L
        const val MAX_PASSWORD = 256L
    }
}