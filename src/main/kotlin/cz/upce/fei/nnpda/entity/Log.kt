package cz.upce.fei.nnpda.entity

import java.util.*
import javax.persistence.*

@Entity
data class Log(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id : Long = 0,
    @ManyToOne val sensor: Sensor,
    @Column val value : Double,
    @Column val timestamp : Date
);