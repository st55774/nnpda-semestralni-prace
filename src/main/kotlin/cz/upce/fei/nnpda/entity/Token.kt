package cz.upce.fei.nnpda.entity

import org.apache.commons.lang3.time.DateUtils
import java.util.*
import javax.persistence.*

@Entity
data class Token(
    @Id @GeneratedValue(strategy = GenerationType.AUTO) val id : Long = 0,
    @ManyToOne val user : User,
    @Column(length = TOKEN_LENGTH.toInt(), nullable = false) val value : String = generateToken(),
    @Column(nullable = false) val expiresAt : Date = DateUtils.addMinutes(Date(), EXPIRATION_TIME)
){
    companion object{
        const val TOKEN_LENGTH = 36L
        const val EXPIRATION_TIME = 5

        /**
         * Generate Token
         *
         * @return the unique UUID
         */
        fun generateToken(): String {
            return UUID.randomUUID().toString()
        }
    }
}