package cz.upce.fei.nnpda.controller

import cz.upce.fei.nnpda.doc.LoginEndpointDoc
import cz.upce.fei.nnpda.doc.RegisterEndpointDoc
import cz.upce.fei.nnpda.dto.request.UserDto
import cz.upce.fei.nnpda.service.exception.UserAlreadyExistException
import cz.upce.fei.nnpda.service.security.AuthenticationService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/user/auth")
@Tag(name = "Authentication", description = "Authentication")
class AuthenticationController(private val authenticationService: AuthenticationService) {

    @PostMapping("/login")
    @LoginEndpointDoc
    fun login(@Valid @RequestBody loginFormDto: UserDto) =
        ResponseEntity.ok(authenticationService.authenticate(loginFormDto))


    @PostMapping("/register")
    @RegisterEndpointDoc
    fun register(@Valid @RequestBody registerFormDto : UserDto) : ResponseEntity<HttpStatus> {
        authenticationService.register(registerFormDto)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @ExceptionHandler(UserAlreadyExistException::class)
    fun userAlreadyExist(ex : UserAlreadyExistException) : ResponseEntity<*> {
        return ResponseEntity.status(HttpStatus.CONFLICT).contentType(MediaType.TEXT_PLAIN).body(ex.message)
    }
}