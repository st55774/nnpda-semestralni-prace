package cz.upce.fei.nnpda.controller

import cz.upce.fei.nnpda.doc.measureMachine.CreateMeasureMachineEndpoint
import cz.upce.fei.nnpda.doc.measureMachine.GetMeasureMachineEndpoint
import cz.upce.fei.nnpda.doc.measureMachine.GetMeasureMachines
import cz.upce.fei.nnpda.doc.measureMachine.UpdateMeasureMachineEndpoint
import cz.upce.fei.nnpda.doc.sensor.*
import cz.upce.fei.nnpda.dto.SensorDto
import cz.upce.fei.nnpda.service.SensorService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid


@RestController
@RequestMapping("/sensor")
@Tag(name = "Sensor", description = "CRUD operations for sensors")
@SecurityRequirement(name = "bearerAuth")
class SensorController(private val sensorService: SensorService) {
    @GetMapping("/")
    @GetMeasureMachines
    fun getAll() = sensorService.findAll()

    @GetMapping("/{id}")
    @GetMeasureMachineEndpoint
    fun get(@PathVariable @Valid id: Long) = sensorService.find(id)

    @PostMapping("/")
    @CreateMeasureMachineEndpoint
    fun post(@RequestBody @Valid sensorDto: SensorDto): ResponseEntity<HttpStatus> {
        sensorService.add(sensorDto)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @PutMapping("/{id}")
    @UpdateMeasureMachineEndpoint
    fun put(@PathVariable @Valid id: Long, @RequestBody @Valid sensorDto: SensorDto) =
        sensorService.update(id, sensorDto)

    @DeleteMapping("/{id}")
    @DeleteMeasureMachineEndpoint
    fun delete(@PathVariable @Valid id: Long) = sensorService.delete(id)
}