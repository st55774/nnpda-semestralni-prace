package cz.upce.fei.nnpda.controller

import cz.upce.fei.nnpda.doc.SetChangePasswordEndpoint
import cz.upce.fei.nnpda.doc.SetResetPasswordEndpoint
import cz.upce.fei.nnpda.doc.StartResetPasswordEndpoint
import cz.upce.fei.nnpda.dto.request.ChangePasswordDto
import cz.upce.fei.nnpda.dto.request.ResetPasswordDto
import cz.upce.fei.nnpda.service.exception.TokenNotValidException
import cz.upce.fei.nnpda.service.security.PasswordService
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/user/password")
@Tag(name = "User Password", description = "User password reset")
class PasswordChangeController(private val passwordService: PasswordService) {

    @GetMapping("/reset/{email}")
    @StartResetPasswordEndpoint
    fun startResetPassword(@PathVariable email: String): ResponseEntity<HttpStatus> {
        passwordService.sendPasswordResetEmail(email)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @PutMapping("/reset/{email}")
    @SetResetPasswordEndpoint
    fun setResetPassword(@PathVariable email: String, @Valid @RequestBody resetPasswordDto: ResetPasswordDto): ResponseEntity<HttpStatus> {
        passwordService.changePassword(email, resetPasswordDto)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @PutMapping("/{email}")
    @SetChangePasswordEndpoint
    fun setPassword(@PathVariable email: String, @Valid @RequestBody changePasswordDto: ChangePasswordDto): ResponseEntity<HttpStatus> {
        passwordService.changePassword(email, changePasswordDto)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @ExceptionHandler(TokenNotValidException::class)
    fun userAlreadyExist(ex: TokenNotValidException): ResponseEntity<*> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.TEXT_PLAIN).body(ex.message)
    }
}