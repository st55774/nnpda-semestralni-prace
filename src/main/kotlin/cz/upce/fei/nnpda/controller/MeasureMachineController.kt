package cz.upce.fei.nnpda.controller

import cz.upce.fei.nnpda.doc.measureMachine.*
import cz.upce.fei.nnpda.doc.sensor.DeleteMeasureMachineEndpoint
import cz.upce.fei.nnpda.dto.MeasureMachineDto
import cz.upce.fei.nnpda.service.MeasureMachineService
import io.swagger.v3.oas.annotations.security.SecurityRequirement
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.NoSuchElementException
import javax.validation.Valid

@RestController
@RequestMapping("/measureMachine")
@Tag(name = "Measure machines", description = "Measure machines")
@SecurityRequirement(name = "bearerAuth")
class MeasureMachineController(private val measureMachineService: MeasureMachineService) {
    @GetMapping("/")
    @GetMeasureMachines
    fun getAll() = measureMachineService.findAll()

    @GetMapping("/{id}")
    @GetMeasureMachineEndpoint
    fun get(@PathVariable @Valid id: Long) = measureMachineService.find(id)

    @PostMapping("/")
    @CreateMeasureMachineEndpoint
    fun post(@RequestBody @Valid measureMachine: MeasureMachineDto): ResponseEntity<HttpStatus> {
        measureMachineService.add(measureMachine)
        return ResponseEntity(HttpStatus.NO_CONTENT)
    }

    @PutMapping("/{id}")
    @UpdateMeasureMachineEndpoint
    fun put(@PathVariable @Valid id: Long, @RequestBody @Valid measureMachine: MeasureMachineDto) =
        measureMachineService.update(id, measureMachine)

    @DeleteMapping("/{id}")
    @DeleteMeasureMachineEndpoint
    fun delete(@PathVariable @Valid id: Long) = measureMachineService.delete(id)

    @PutMapping("/{idMachine}/sensor/{idSensor}")
    @LinkMeasureMachineSensorEndpoint
    fun put(@PathVariable @Valid idMachine: Long, @PathVariable @Valid idSensor: Long) =
        measureMachineService.link(idMachine, idSensor)

    @DeleteMapping("/{idMachine}/sensor/{idSensor}")
    @UnlinkMeasureMachineSensorEndpoint
    fun delete(@PathVariable @Valid idMachine: Long, @PathVariable @Valid idSensor: Long) =
        measureMachineService.unlink(idMachine, idSensor)

    @ExceptionHandler(NoSuchElementException::class)
    fun userAlreadyExist(ex: NoSuchElementException): ResponseEntity<*> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).contentType(MediaType.TEXT_PLAIN).body(ex.message)
    }
}