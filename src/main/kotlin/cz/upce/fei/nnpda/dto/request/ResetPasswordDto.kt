package cz.upce.fei.nnpda.dto.request

import cz.upce.fei.nnpda.entity.Token
import cz.upce.fei.nnpda.entity.User
import javax.validation.constraints.Max
import javax.validation.constraints.Min

data class ResetPasswordDto(
    @Min(User.MIN_PASSWORD)
    @Max(User.MAX_PASSWORD)
    val password : String = "",

    @Min(Token.TOKEN_LENGTH)
    @Max(Token.TOKEN_LENGTH)
    val token : String = "",
)
