package cz.upce.fei.nnpda.dto

import cz.upce.fei.nnpda.entity.MeasureMachine
import javax.validation.constraints.Max
import javax.validation.constraints.Min

class SensorDto (
    @Min(MeasureMachine.MAX_LENGTH)
    @Max(MeasureMachine.MAX_LENGTH)
    val description : String = ""
)