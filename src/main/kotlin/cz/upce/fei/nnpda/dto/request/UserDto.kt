package cz.upce.fei.nnpda.dto.request

import cz.upce.fei.nnpda.entity.User
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Pattern

data class UserDto(
    @Min(User.MIN_PASSWORD)
    @Max(User.MAX_PASSWORD)
    var password : String = "",

    @Pattern(regexp = "^(.+)@(.+)\$")
    var email : String = "",
)