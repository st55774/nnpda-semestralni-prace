package cz.upce.fei.nnpda.dto.response

data class JwtResponse(
    var accessToken: String,
    var email: String,
    var tokenType: String = "Bearer"
)