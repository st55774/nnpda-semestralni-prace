package cz.upce.fei.nnpda.dto.request

import cz.upce.fei.nnpda.entity.User
import javax.validation.constraints.Max
import javax.validation.constraints.Min

class ChangePasswordDto (
    @Min(User.MIN_PASSWORD)
    @Max(User.MAX_PASSWORD)
    val password : String = "",

    @Min(User.MIN_PASSWORD)
    @Max(User.MAX_PASSWORD)
    val newPassword : String = ""
)