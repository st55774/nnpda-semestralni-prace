package cz.upce.fei.nnpda.service.security.principles

import com.fasterxml.jackson.annotation.JsonIgnore
import cz.upce.fei.nnpda.entity.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails

class UserPrinciple(
    private var id : Long,
    private var email : String,
    @JsonIgnore private var password : String
) : UserDetails{
    override fun getAuthorities() = mutableListOf<GrantedAuthority>()

    override fun getPassword() = password

    override fun getUsername() = email

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UserPrinciple

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }

    companion object{
        fun build(user : User) : UserPrinciple {
            return UserPrinciple(user.id, user.email, user.password)
        }
    }
}