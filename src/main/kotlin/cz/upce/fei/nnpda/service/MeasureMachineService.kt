package cz.upce.fei.nnpda.service

import cz.upce.fei.nnpda.dto.MeasureMachineDto
import cz.upce.fei.nnpda.entity.MeasureMachine
import cz.upce.fei.nnpda.repository.MeasureMachineRepository
import cz.upce.fei.nnpda.repository.SensorRepository
import cz.upce.fei.nnpda.service.security.UserAuthenticationService
import org.springframework.stereotype.Service

@Service
class MeasureMachineService(
    private val userAuthenticationService: UserAuthenticationService,
    private val measureMachineRepository: MeasureMachineRepository,
    private val sensorRepository: SensorRepository
) {
    fun findAll(): List<MeasureMachine> =
        measureMachineRepository.findAllByUserEquals(userAuthenticationService.getLoggedUser())


    fun find(id: Long): MeasureMachine =
        measureMachineRepository.findByIdAndUserEquals(id, userAuthenticationService.getLoggedUser()).orElseThrow()

    fun add(measureMachineDto: MeasureMachineDto) =
        measureMachineRepository.save(
            MeasureMachine(
                description = measureMachineDto.description,
                user = userAuthenticationService.getLoggedUser()
            )
        )

    fun update(id: Long, measureMachineDto: MeasureMachineDto) {
        val measureMachine =
            measureMachineRepository.findByIdAndUserEquals(id, userAuthenticationService.getLoggedUser())
                .orElseThrow()

        measureMachine.description = measureMachine.description
        measureMachineRepository.save(measureMachine)
    }

    fun delete(id: Long) {
        val measureMachine =
            measureMachineRepository.findByIdAndUserEquals(id, userAuthenticationService.getLoggedUser())
                .orElseThrow()

        measureMachineRepository.delete(measureMachine)
    }

    fun link(idMachine: Long, idSensor: Long) {
        val measureMachine =
            measureMachineRepository.findByIdAndUserEquals(idMachine, userAuthenticationService.getLoggedUser())
                .orElseThrow()
        val sensor = sensorRepository.findById(idSensor).orElseThrow()

        measureMachine.sensors.add(sensor)
        measureMachineRepository.save(measureMachine)
    }

    fun unlink(idMachine: Long, idSensor: Long) {
        val measureMachine =
            measureMachineRepository.findByIdAndUserEquals(idMachine, userAuthenticationService.getLoggedUser())
                .orElseThrow()
        val sensor = sensorRepository.findById(idSensor).orElseThrow()

        measureMachine.sensors.remove(sensor)
        measureMachineRepository.save(measureMachine)
    }
}
