package cz.upce.fei.nnpda.service

import cz.upce.fei.nnpda.dto.SensorDto
import cz.upce.fei.nnpda.entity.Sensor
import cz.upce.fei.nnpda.repository.SensorRepository
import org.springframework.stereotype.Service

@Service
class SensorService(private val sensorRepository: SensorRepository) {
    fun findAll() = sensorRepository.findAll()

    fun find(id: Long) = sensorRepository.findById(id).orElseThrow()

    fun update(id: Long, sensorDto: SensorDto) {
        val sensor = sensorRepository.findById(id).orElseThrow()

        sensor.description = sensor.description
        sensorRepository.save(sensor)
    }

    fun add(sensor: SensorDto) =
        sensorRepository.save(Sensor(description = sensor.description))


    fun delete(id: Long) = sensorRepository.deleteById(id)
}
