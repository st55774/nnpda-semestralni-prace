package cz.upce.fei.nnpda.service.security

import cz.upce.fei.nnpda.component.JwtTokenUtility
import cz.upce.fei.nnpda.dto.request.UserDto
import cz.upce.fei.nnpda.dto.response.JwtResponse
import cz.upce.fei.nnpda.entity.User
import cz.upce.fei.nnpda.repository.UserRepository
import cz.upce.fei.nnpda.service.exception.UserAlreadyExistException
import cz.upce.fei.nnpda.service.security.principles.UserPrinciple
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserAuthenticationService(
    private val userRepository: UserRepository,
    private val encoder: PasswordEncoder,
    private val authenticationManager: AuthenticationManager,
    private val jwtTokenUtility: JwtTokenUtility
) : AuthenticationService {

    override fun authenticate(userDto: UserDto): JwtResponse {
        val authentication = authenticationManager.authenticate(
            UsernamePasswordAuthenticationToken(userDto.email, userDto.password)
        )

        SecurityContextHolder.getContext().authentication = authentication

        val accessToken = jwtTokenUtility.generateJwtToken(authentication)
        val userDetail = authentication.principal as UserDetails
        return JwtResponse(accessToken, userDetail.username)
    }

    override fun register(userDto: UserDto) {
        if(userRepository.existsByEmail(userDto.email)) throw UserAlreadyExistException(userDto.email)
        userRepository.save(User(email = userDto.email, password = encoder.encode(userDto.password)))
    }

    override fun getLoggedUser(): User {
        val logged = SecurityContextHolder.getContext().authentication.principal as UserPrinciple

        return userRepository.findByEmail(logged.username).get()
    }
}