package cz.upce.fei.nnpda.service.security

import cz.upce.fei.nnpda.dto.request.ChangePasswordDto
import cz.upce.fei.nnpda.dto.request.ResetPasswordDto
import cz.upce.fei.nnpda.entity.Token
import cz.upce.fei.nnpda.entity.User
import cz.upce.fei.nnpda.repository.TokenRepository
import cz.upce.fei.nnpda.repository.UserRepository
import cz.upce.fei.nnpda.service.email.EmailService
import cz.upce.fei.nnpda.service.exception.TokenNotValidException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class PasswordService(
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository,
    private val passwordEncoder: PasswordEncoder,
    private val emailService: EmailService
) {

    fun sendPasswordResetEmail(email: String) {
        val user = findUserBasedOnEmail(email)

        val token = tokenRepository.save(Token(user = user))
        emailService.sendSimpleMessage(email, token.value)

        log.info("New email with ${token.value} token reset was send to user $email.")
    }

    fun changePassword(email : String, resetPasswordDto: ResetPasswordDto){
        val user = findUserBasedOnEmail(email)
        tokenRepository.delete(validateToken(resetPasswordDto, user))

        updatePassword(resetPasswordDto.password, user)
    }

    fun changePassword(email: String, changePasswordDto: ChangePasswordDto) {
        val user = findUserBasedOnEmail(email)
        validatePassword(changePasswordDto, user)

        updatePassword(changePasswordDto.newPassword, user)
    }

    private fun updatePassword(password : String, user: User){
        user.password = passwordEncoder.encode(password)
        userRepository.save(user)
    }

    private fun validatePassword(changePasswordDto: ChangePasswordDto, user: User) =
        changePasswordDto.password == user.password

    private fun validateToken(resetPasswordDto: ResetPasswordDto, user: User): Token {
        val token = tokenRepository.findByValueAndUser(resetPasswordDto.token, user)

        if (!token.isPresent || Date().after(token.get().expiresAt)) {
            throw TokenNotValidException()
        }

        return token.get()
    }

    private fun findUserBasedOnEmail(email: String): User {
        val user = userRepository.findByEmail(email)

        if (!user.isPresent) {
            throw UsernameNotFoundException(email)
        }
        return user.get()
    }


    private companion object {
        val log: Logger = LoggerFactory.getLogger(PasswordService::class.java)
    }
}