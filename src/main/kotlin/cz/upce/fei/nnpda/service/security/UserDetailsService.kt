package cz.upce.fei.nnpda.service.security

import cz.upce.fei.nnpda.entity.User
import cz.upce.fei.nnpda.repository.UserRepository
import cz.upce.fei.nnpda.service.security.principles.UserPrinciple

import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserDetailsService(private var userRepository: UserRepository) : UserDetailsService {
    @Transactional
    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(username: String): UserDetails {
        val user: User = userRepository.findByEmail(username)
            .orElseThrow { UsernameNotFoundException("User Not Found with -> username or email : $username") }
        return UserPrinciple.build(user)
    }
}