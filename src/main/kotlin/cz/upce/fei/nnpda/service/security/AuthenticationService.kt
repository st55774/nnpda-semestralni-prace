package cz.upce.fei.nnpda.service.security

import cz.upce.fei.nnpda.dto.request.UserDto
import cz.upce.fei.nnpda.dto.response.JwtResponse
import cz.upce.fei.nnpda.entity.User

interface AuthenticationService {
    fun authenticate(userDto: UserDto): JwtResponse
    fun register(userDto: UserDto)
    fun getLoggedUser(): User
}