package cz.upce.fei.nnpda.service.data

import cz.upce.fei.nnpda.entity.Log
import cz.upce.fei.nnpda.entity.Sensor
import cz.upce.fei.nnpda.entity.SensorType
import cz.upce.fei.nnpda.repository.LogRepository
import cz.upce.fei.nnpda.repository.SensorRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Scope
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.*

@Service
@Scope("singleton")
class DataGenerator(
    private val logRepository: LogRepository,
    private val sensorRepository: SensorRepository
) {
    @Scheduled(fixedDelay = 7000)
    fun generateData() {
        log.info("New generating sequence started at ${Date()}")

        (0..(randomGenerator.nextInt(MAX_COUNT - MIN_COUNT) + MIN_COUNT))
            .forEach { _ ->
                val sensor = pickRandomSensor()
                try {
                    logRepository.save(
                        Log(
                            sensor = sensor,
                            value = createValueBasedOnSensor(sensor),
                            timestamp = Date()
                        )
                    )
                } catch (ex: NoSuchElementException) {
                }
            }
    }

    private fun pickRandomSensor(): Sensor {
        val sensorIdSet = sensorRepository.findIdSet()
        val id = sensorIdSet[Random().nextInt(sensorIdSet.size)]

        return sensorRepository.findById(id).get()
    }

    private fun createValueBasedOnSensor(sensor: Sensor) =
        when(sensor.type){
            SensorType.ELECTRICITY -> randomGenerator.nextInt(EL_MAX).toDouble()
            SensorType.WATER -> randomGenerator.nextInt(WATER_MAX).toDouble()
            SensorType.AIR -> randomGenerator.nextInt(AIR_MAX).toDouble()
        }


    private companion object {
        const val MIN_COUNT = 4
        const val MAX_COUNT = 8

        const val EL_MAX = 100
        const val WATER_MAX = 120
        const val AIR_MAX = 10

        val log: Logger = LoggerFactory.getLogger(DataGenerator::class.java)
        val randomGenerator = Random()
    }
}