package cz.upce.fei.nnpda.service.email

interface EmailService {
    fun sendSimpleMessage(to: String, token: String)
}