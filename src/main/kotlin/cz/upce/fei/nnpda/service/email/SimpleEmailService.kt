package cz.upce.fei.nnpda.service.email

import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
class SimpleEmailService(private val mailSender: JavaMailSender) : EmailService {
    @Value("\${server.url}")
    private var url = ""

    @Value("\${spring.mail.username}")
    private var from = ""

    override fun sendSimpleMessage(to : String, token : String) {
        val message = SimpleMailMessage()
        message.setFrom(from)
        message.setTo(to)
        message.setSubject(SUBJECT)
        message.setText(token)
        mailSender.send(message)
    }

    private companion object{
        const val SUBJECT = "Password Reset token"
    }
}