package cz.upce.fei.nnpda.service

import cz.upce.fei.nnpda.repository.LogRepository
import org.springframework.stereotype.Service

@Service
class LogService(private val logRepository: LogRepository) {
    fun findAll() = logRepository.findAll()
    fun findById(id : Long) = logRepository.findById(id).orElseThrow()
}
