package cz.upce.fei.nnpda.service.exception

class TokenNotValidException : Throwable("Token not valid!")