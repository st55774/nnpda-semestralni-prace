package cz.upce.fei.nnpda.service.exception

class UserAlreadyExistException(email: String) : Throwable("User $email is already registered.")