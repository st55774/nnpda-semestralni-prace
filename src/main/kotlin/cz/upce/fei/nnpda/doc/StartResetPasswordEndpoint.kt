package cz.upce.fei.nnpda.doc

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.MediaType
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@Operation(summary = "Start password reset process")
@ApiResponses(
    ApiResponse(
        responseCode = "204",
        description = "Sending password reset email success",
    ),
    ApiResponse(
        responseCode = "404",
        description = "Email not found",
        content = [Content(mediaType = MediaType.TEXT_PLAIN_VALUE)]
    )
)
annotation class StartResetPasswordEndpoint
