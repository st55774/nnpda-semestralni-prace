package cz.upce.fei.nnpda.doc.measureMachine

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.MediaType
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@Operation(summary = "Create measure machine")
@ApiResponses(
    ApiResponse(
        responseCode = "204",
        description = "Success",
        content = [Content(mediaType = MediaType.TEXT_PLAIN_VALUE)]),
    ApiResponse(
        responseCode = "400",
        description = "Check your request body",
        content = [Content(mediaType = MediaType.TEXT_PLAIN_VALUE)]
    )
)
annotation class CreateMeasureMachineEndpoint