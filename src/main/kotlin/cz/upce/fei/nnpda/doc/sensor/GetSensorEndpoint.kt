package cz.upce.fei.nnpda.doc.sensor

import cz.upce.fei.nnpda.entity.Sensor
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.springframework.http.MediaType
import java.lang.annotation.Inherited

@Target(AnnotationTarget.FUNCTION)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Inherited
@Operation(summary = "Provide sensor by id")
@ApiResponses(
    ApiResponse(
        responseCode = "200",
        description = "Success",
        content = [Content(
            mediaType = MediaType.APPLICATION_JSON_VALUE,
            schema = Schema(implementation = Sensor::class)
        )]
    ),
    ApiResponse(
        responseCode = "404",
        description = "Not found",
        content = [Content(mediaType = MediaType.TEXT_PLAIN_VALUE)]
    )
)
annotation class GetSensorEndpoint