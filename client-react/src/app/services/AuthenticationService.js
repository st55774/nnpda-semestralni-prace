import axios from "axios";

const SERVER_PREFIX = process.env.REACT_APP_BASE_URI

const AuthenticationService = {

  signIn: function (username, password) {
    return axios
      .post(`${SERVER_PREFIX}/user/auth/login`, { "email" : username, "password" : password })
      .then((response) => {
        if (response.data.accessToken) {
          localStorage.setItem("user", JSON.stringify(response.data));

          const token = this.getCurrentUser().accessToken;
          localStorage.setItem("accessToken", token);
        }
        return response.data;
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  },

  signOut: function () {
    localStorage.removeItem("user");
  },

  register: function async(firstname, lastname, username, email, password) {
    return axios.post(`${SERVER_PREFIX}/api/auth/signup`, {
      firstname,
      lastname,
      username,
      email,
      password,
    });
  },

  getCurrentUser: function () {
    return JSON.parse(localStorage.getItem("user"));
  },

  isUserLogin: function () {
    return !!localStorage.getItem("user");
  },

  getUsername: function () {
    return JSON.parse(localStorage.getItem("user")).username;
  },

  getAuthorities: function () {
    return JSON.parse(localStorage.getItem("user")).authorities;
  }
};

export default AuthenticationService;
