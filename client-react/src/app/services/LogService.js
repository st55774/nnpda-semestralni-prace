
import axios from "axios";

axios.interceptors.request.use((config) => {
    const user = JSON.parse(localStorage.getItem("user"));

    if (user && user.accessToken) {
        const token = "Bearer " + user.accessToken;
        config.headers.Authorization = token;
    }

    return config;
});

const SERVER_PREFIX = process.env.REACT_APP_BASE_URI

const LogService = {
    getLogs : async function(page, size){
        return await axios.get(`${SERVER_PREFIX}/logs?page=${page}&size=${size}`)
    }
}

export default LogService;