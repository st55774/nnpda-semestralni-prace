import {Alert} from "react-bootstrap";
import AppNavbar from "../navbar/AppNavbar";
import {Container} from "reactstrap";
import React, {useEffect, useState} from "react";
import BootstrapTable from "react-bootstrap-table-next";
import filterFactory from 'react-bootstrap-table2-filter';
import LogService from "../../services/LogService";
import paginationFactory from 'react-bootstrap-table2-paginator';

function LogList() {
    const [logs, setLogs] = useState([]);
    const [page, setPage] = useState(0);
    const [size, setSize] = useState(0);
    const [total, setTotal] = useState(0);

    const onPageChange = (page, sizePerPage) => {
        LogService.getLogs(page, 20).then((result) => {
            if (result) {
                setLogs(result.data["_embedded"]["logs"]);
                setSize(result.data.page.totalPages);
                setPage(result.data.page.number)
                setTotal(result.data.page.totalElements)
            }
        });
    }

    const remote = {
        filter: false,
        pagination: true,
        sort: false,
        cellEdit: false
    };

    const sensorTypes = {
        'ELECTRICITY': 'ELECTRICITY',
        'WATER': 'WATER',
        'WIND': 'WIND'
    };

    const columns = [{
        dataField: 'id',
        text: 'Id',
        //filter: textFilter()
    }, {
        dataField: 'sensor.description',
        text: 'Název senzoru',
        //filter: textFilter()
    }, {
        dataField: 'sensor.type',
        text: 'Typ senzoru',
        //filter: selectFilter({
        //    options: () => sensorTypes
        //})
    }, {
        dataField: 'value',
        text: 'Naměřená hodnota',
    }, {
        dataField: 'timestamp',
        text: 'Čas měření',
    }];

    useEffect(() => {
        LogService.getLogs(page, 20).then((result) => {
            if (result) {
                setLogs(result.data["_embedded"]["logs"]);
                setSize(result.data.page.totalPages);
                setPage(result.data.page.number)
                setTotal(result.data.page.totalElements)
            }
        });
    }, [page]);

    const onTableChange = (type, newState) => {

    };

    let detailList = (
        <div>
            <Alert variant="info">
                <h1>Logy</h1>
            </Alert>

            <div>
                <BootstrapTable filter={filterFactory()} pagination={paginationFactory({
                    page: page,
                    totalSize: total,
                    hideSizePerPage: true,
                    sizePerPage: 20,
                    lastPageText: '>>',
                    firstPageText: '<<',
                    nextPageText: '>',
                    prePageText: '<',
                    showTotal: true,
                    alwaysShowAllBtns: true,
                    onPageChange: onPageChange
                })} keyField='id' remote={remote} onTableChange={onTableChange} columns={columns} data={logs}/>
            </div>
        </div>
    );

    return (
        <div>
            <AppNavbar/>
            <Container fluid>{detailList}</Container>
        </div>
    );
}

export default LogList;