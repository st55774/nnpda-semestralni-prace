import AppNavbar from "../navbar/AppNavbar";
import { Container } from "reactstrap";
import { Button, Form, FormGroup, Input, Label, Row, Col } from "reactstrap";
import { Alert } from "react-bootstrap";

import Authentication from "../../services/AuthenticationService";
import React, { useState } from "react";

const validEmailRegex = RegExp(
  //eslint-disable-next-line
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const validateForm = (errors) => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

function SignUp(props) {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [validForm, setValidForm] = useState(true);
  const [errorFirstName, setErrorFirstName] = useState("");
  const [errorLastName, setErrorLastnName] = useState("");
  const [errorUserName, setErrorUserName] = useState("");
  const [errorEmail, setErrorEmail] = useState("");
  const [errorPassword, setErrorPassoword] = useState("");

  const handleFirstNameInput = (e) => {
    setFirstName(e.target.value);
    setErrorFirstName(
      e.target.value.length < 3 ? "Jméno musí mít minimálně 3 znaky!" : ""
    );
  };

  const handleLastNameInput = (e) => {
    setLastName(e.target.value);
    setErrorLastnName(
      e.target.value.length < 3 ? "Příjmení musí mít minimálně 3 znaky!" : ""
    );
    console.log(errorLastName);
  };

  const handleUserNameInput = (e) => {
    setUserName(e.target.value);
    setErrorUserName(
      e.target.value.length < 5 ? "Uživatelské jméno musí mít minimálně 5 znaků!" : ""
    );
  };

  const handleEmailInput = (e) => {
    setEmail(e.target.value);
    setErrorEmail(
      validEmailRegex.test(e.target.value) ? "" : "E-mail není validní!"
    );
  };

  const handlePasswordInput = (e) => {
    setPassword(e.target.value);
    setErrorPassoword(
      e.target.value.length < 8 ? "Heslo musí být minimálně 8 znaků dlouhé!" : ""
    );
  };

  const signUp = (e) => {
    e.preventDefault();

    let errors = {
      firstname: errorFirstName,
      lastName: errorLastName,
      userName: errorUserName,
      email: errorEmail,
      password: errorPassword,
    };
    const valid = validateForm(errors);

    setValidForm(valid);
    if (valid) {
      Authentication.register(
        firstName,
        lastName,
        userName,
        email,
        password
      ).then(
        (response) => {
          setMessage(response.data.message);
          setSuccessful(true);

          setTimeout(() => {
            props.history.push("/signin");
        }, 3000);
        },
        (error) => {
          console.log("Fail! Error = " + error.toString());

          setMessage(error.toString());
          setSuccessful(false);
        }
      );
    }
  };

  const title = <h2>Vytvořit nový účet</h2>;
  let alert = "";

  if (message) {
    if (successful) {
      alert = <Alert variant="success">{message}</Alert>;
    } else {
      alert = <Alert variant="danger">{message}</Alert>;
    }
  }

  return (
    <div>
      <AppNavbar />
      <Container fluid>
        <Row style={{marginTop: "20px"}}>
          <Col sm="12" md={{ size: 4, offset: 4 }}>
            {title}
            <Form onSubmit={signUp}>
              <FormGroup controlId="forFirstname">
                <Label for="firstname">Jméno</Label>
                <Input
                  required
                  type="text"
                  placeholder="Zadejte jméno"
                  name="firstname"
                  id="firstname"
                  value={firstName}
                  autoComplete="firstname"
                  onChange={handleFirstNameInput}
                />
                {errorFirstName && (
                  <Alert variant="danger">{errorFirstName}</Alert>
                )}
              </FormGroup>

              <FormGroup controlId="forLastname">
                <Label for="lastname">Příjmení</Label>
                <Input
                  required
                  type="text"
                  placeholder="Zadejte příjmení"
                  name="lastname"
                  id="lastname"
                  value={lastName}
                  autoComplete="lastname"
                  onChange={handleLastNameInput}
                />
                {errorLastName && <Alert variant="danger">{errorLastName}</Alert>}
              </FormGroup>

              <FormGroup controlId="forUsername">
                <Label for="username">Uživatelské jméno</Label>
                <Input
                  required
                  type="text"
                  placeholder="Zadejte uživatelské jméno"
                  name="username"
                  id="username"
                  value={userName}
                  autoComplete="username"
                  onChange={handleUserNameInput}
                />
                {errorUserName && <Alert variant="danger">{errorUserName}</Alert>}
              </FormGroup>

              <FormGroup controlId="formEmail">
                <Label for="email">E-mail</Label>
                <Input
                  required
                  type="text"
                  placeholder="Zadejte e-mail"
                  name="email"
                  id="email"
                  value={email}
                  autoComplete="email"
                  onChange={handleEmailInput}
                />
                {errorEmail && <Alert variant="danger">{errorEmail}</Alert>}
              </FormGroup>

              <FormGroup controlId="formPassword">
                <Label for="password">Heslo</Label>
                <Input
                  required
                  type="password"
                  placeholder="Zadejte heslo"
                  name="password"
                  id="password"
                  value={password}
                  autoComplete="password"
                  onChange={handlePasswordInput}
                />
                {errorPassword && (
                  <Alert key="errorspassword" variant="danger">
                    {errorPassword}
                  </Alert>
                )}
              </FormGroup>

              <Button variant="primary" type="submit">
                Vytvořit účet
              </Button>
              {!validForm && (
                <Alert key="validForm" variant="danger">
                  Prosím zkontrolujte zadané hodnoty!
                </Alert>
              )}
              {alert}
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default SignUp;
