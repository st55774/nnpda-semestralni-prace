import {
    Collapse,
    Nav,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavbarText,
    NavItem,
    NavLink,
} from "reactstrap";
import {Link} from "react-router-dom";
import {withRouter} from "react-router-dom";
import AuthenticationService from "../../services/AuthenticationService";
import React, {useState, useEffect} from "react";

function AppNavbar(props) {
    const [showUser, setShowUser] = useState(false);
    const [showAdmin, setShowAdmin] = useState(false);
    const [userName, setUserName] = useState(undefined);
    const [login, setLogin] = useState(false);
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        const user = AuthenticationService.getCurrentUser();

        if (user) {
            setShowUser(true);
            setLogin(true);
            setUserName(user.username);
        }
    }, []);

    const signOut = () => {
        AuthenticationService.signOut();
        props.history.push("/home");
        window.location.reload();
    };

    const toggle = () => {
        setIsOpen(!isOpen);
    };

    return (
        <Navbar color="dark" dark expand="md">
            <NavbarBrand tag={Link} to="/home">
                IBS
            </NavbarBrand>
            <Nav className="mr-auto">
                <NavLink href="/#/home">Domů</NavLink>
                <NavLink href="/#/logs">Logy</NavLink>
            </Nav>
            <NavbarToggler onClick={toggle}/>
            <Collapse isOpen={isOpen} navbar>
                {login ? (
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="#" onClick={signOut}>
                                Odhlásit se
                            </NavLink>
                        </NavItem>
                    </Nav>
                ) : (
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink href="/#/signin">Přihlásit se</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink href="/#/signup">Vytvořit účet</NavLink>
                        </NavItem>
                    </Nav>
                )}
            </Collapse>
        </Navbar>
    );
}

export default withRouter(AppNavbar);
