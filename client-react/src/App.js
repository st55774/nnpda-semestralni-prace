import "./App.css";

import {HashRouter, Route, Switch} from "react-router-dom";
import PrivateRoute from './app/components/route/PrivateRoute'
import Home from "./app/components/home/Home";
import SignUp from "./app/components/auth/SignUp";
import Login from "./app/components/auth/Login";
import Profile from "./app/components/profile/Profile";
import LogList from "./app/components/log/LogList";

function App() {
    return (
        <HashRouter>
            <Switch>
                <Route path="/" exact={true} component={Home}/>
                <Route path="/home" exact={true} component={Home}/>
                <Route path="/signin" exact={true} component={Login}/>
                <Route path="/signup" exact={true} component={SignUp}/>
                <Route path="/logs" exact={true} component={LogList}/>
                <PrivateRoute path="/profile" exact={true} component={Profile}/>
            </Switch>
        </HashRouter>
    )
}

export default App;